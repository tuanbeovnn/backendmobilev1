package mobileweb.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mobileweb.converter.ProductConverter;
import mobileweb.dto.ProductDTO;
import mobileweb.entity.ProducerEntity;
import mobileweb.entity.ProductEntity;
import mobileweb.repository.ProducerRepository;
import mobileweb.repository.ProductRepository;
import mobileweb.service.IProductService;

@Service
public class ProductService implements IProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProducerRepository producerRepository;
	
	@Autowired
	private ProductConverter productConverter;
	@Override
	public ProductDTO save(ProductDTO productDTO) {
		ProductEntity productEntity = new ProductEntity();
		if(productDTO.getId()!=null) {
			ProductEntity oldproductEntity = productRepository.findOne(productDTO.getId());
			productEntity = productConverter.toEntity(productDTO,oldproductEntity);
		}else {
			productEntity = productConverter.toEntity(productDTO);
		}
		
		ProducerEntity producerEntity = producerRepository.findByCode(productDTO.getProducerCode());
		
		productEntity.setProducer(producerEntity);
		productEntity = productRepository.save(productEntity);
		
		return productConverter.toDto(productEntity);
	}
//	@Override
//	public ProductDTO update(ProductDTO productDTO) {
//		ProductEntity oldProductEntity = productRepository.findOne(productDTO.getId());
//		ProductEntity newProductEntity = productConverter.toEntity(productDTO,oldProductEntity);
//		ProductEntity productEntity = productConverter.toEntity(productDTO, oldProductEntity);
//		ProducerEntity producerEntity = producerRepository.findByCode(productDTO.getProducerCode());
//		productEntity = productRepository.save(productEntity);
//		return productConverter.toDto(productEntity);
//	}

@Override
public List<ProductDTO> findAll() {
	List<ProductDTO> results = new ArrayList<>();
	List<ProductEntity> entities = productRepository.findAll();
	for( ProductEntity item : entities) {
		ProductDTO productDTO = productConverter.toDto(item);
		results.add(productDTO);
	}
	
	return results;
}

	@Override
	public ProductEntity getProductById(Long id) {
		// TODO Auto-generated method stub
		ProductEntity productEntity = productRepository.findById(id);
		
		return productEntity;
	}
	@Override
	public void delete(long id) {
		productRepository.delete(id);
	}

//	@Override
//	public ProductDTO update(ProductDTO productDTO) {
//		// TODO Auto-generated method stub
//		return null;
//	}




	
	
}
